
from __future__ import annotations
from abc import ABCMeta, abstractmethod
import logging

from typing import Callable, Dict, Tuple




class ConnectionFactory:
    registry: Dict[str, ConnectionBase] = {}
    
    @classmethod
    def register(cls, name: str) -> Callable:
        def wrapper(wrapped_class: ConnectionBase) -> Callable:
            if name in cls.registry:
                logging.warning(f'{name} already in registry, replacing')
            cls.registry[name] = wrapped_class
            return wrapped_class
        
        return wrapper

    @classmethod
    def create_connection(cls, name: str, **kwargs) -> ConnectionBase:
        """Factory method"""

        if name not in cls.registry:
            logging.warning(f'{name} not in registry')
            return None
        conn_class = cls.registry[name]
        conn = conn_class(**kwargs)
        return conn


class ConnectionBase(metaclass=ABCMeta):
    def __init__(self, **kwargs):
        """ Constructor """

    @abstractmethod
    def run(self, command: str) -> Tuple[str, str]:
        """ Abstract method to run a command """


@ConnectionFactory.register('Arista_SSH')
@ConnectionFactory.register('Fondry_SSH')
class AgnosticConnection(ConnectionBase):
    ...


@ConnectionFactory.register('Juniper_SSH')
class JuniperSSH(ConnectionBase):
    ...


def action(conf):
    conn = ConnectionFactory.create_connection(conf['transport'])
    conn.run('show vlan')
