from __future__ import annotations
from abc import ABC, abstractclassmethod, abstractmethod, abstractstaticmethod
from copy import copy, deepcopy


class Commands:
    ...
class Connection:
    ...

class AddIpAction:
    def __init__(self, scenario, config):
        self.scenario = scenario
        self.conn = Connection(config)

    def run(self) -> list:
        for ip in self.args:
            res = self.conn.execute(self.commands.add_ip(ip))
            self.scenario.result.append(res)
        return self.scenario.result




# Prototype
class AbstractSessionScenario(ABC):
    def __init__(self, args):
        self.args = args
        self.commands = Commands()
        self.results = []

    @abstractmethod
    def clone(self):
        ...


class ConcreteSessionScenario(AbstractSessionScenario):
    def clone(self):  
        cls = self.__class__
        new = cls.__new__(cls)
        new.__dict__.update(self.__dict__)
        # Атрибути в новому об'єкті будуть посиланнями на атрибути оригінуалу 
        super(ConcreteSessionScenario, new).__init__(self.args)
        
        # Напримклад Нам потрібно посилання на атрибути оригінплу окрім results, тому тут створємо новий масив і переприсвоюємо його в атрибут 
        new.results = []
        return new        



def add_ip(args, configs):
    scenario = ConcreteSessionScenario(args)
    scenarios = []
    add_ip_action_results = []
    for device_conf in configs:
        device_scenario = scenario.clone()
        scenarios.append(device_scenario)

        device_result = AddIpAction(device_scenario, device_conf).run()
        add_ip_action_results.append(device_result)
    

    DB.scenarios.bulk_create(scenarios)

    DB.devices.bulk_update(add_ip_action_results)
    

    


