from __future__ import annotations
from typing import Tuple, Dict, Union, List
from abc import ABC, abstractclassmethod, abstractmethod, abstractstaticmethod


class AbstractTransport:
    @abstractmethod
    def connect(self):
        ...
    
    @abstractmethod
    def write(self) -> str:
        ...

    @abstractmethod
    def run_cmds(self) -> Dict[str, Union[Dict, List]]:
        ...


class AbstractCommands:

    @abstractmethod
    def create_vlan(self) -> Tuple[str]:
        ...
    
    @abstractmethod
    def remove_vlan(self) -> Tuple[str]:
        ...


class AbstractConnection:

    def __init__(self, factory: AbstractFactory) -> None:
        self.transport = factory.create_transport()  # Dependency Injection (kind of)
        self.commands = factory.create_commands()

    @abstractmethod
    def create_vlan(self) -> str:
        self.transport.send(self.comands.create_vlan())  # Bridge pattern
        ...
    
    @abstractmethod
    def remove_vlan(self) -> str:
        ...


class AbstractFactory(ABC):

    @abstractstaticmethod
    def create_transport(self) -> AbstractTransport:
        ...

    @abstractstaticmethod
    def create_commands(self) -> AbstractCommands:
        ...

    @abstractclassmethod
    def create_connection(self) -> AbstractConnection:
        ...


######################################## Concrete Factories #########################################

class AbstractAristaFactory(AbstractFactory):
    @staticmethod
    def create_commands() -> AristaConcreteComands:
        return AristaConcreteComands()


class AsristaConcreteSSHConnectionFactory(AbstractAristaFactory):
    @staticmethod
    def create_transport() -> ConcreteSSHTransaport:
        return ConcreteSSHTransaport()

    @classmethod
    def create_connection(cls) -> ConcreteSSHConnection:  # Factory Method
        return ConcreteSSHConnection(cls)


class AsristaConcreteJRPCConnectionFactory(AbstractAristaFactory):
    @staticmethod
    def create_transport() -> ConcreteJRPCTansport:
        return ConcreteJRPCTansport()

    @classmethod
    def create_connection(cls) -> ConcreteJRPCConnection:  # Factory Method
        return ConcreteJRPCConnection(cls)



########################################## Client Code ########################################

def create_vlan_action(factory: AbstractFactory):

    connection = factory.create_connection()
    connection.create_vlan()



class CreateVlanController:

    def take_action(self, args) -> None:

        if args.jrpc:
            factory = AsristaConcreteJRPCConnectionFactory
        elif args.shh:
            factory = AsristaConcreteSSHConnectionFactory
        else:
            raise Exception

        create_vlan_action(factory)

################################################################################################





############################# Concrete End classes Implementations #########################################
class ConcreteJRPCTansport(AbstractTransport):
    ...


class ConcreteSSHTransaport(AbstractTransport):
    ...


class ConcreteSSHConnection(AbstractConnection):
    ...

class ConcreteJRPCConnection(AbstractConnection):
    ...


class AristaConcreteComands(AbstractCommands):
    ...
