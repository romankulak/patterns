from __future__ import annotations
import copy
import time
from ipaddress import ip_address
from typing import Dict, Generator



class Transport:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
    
    def sendline(self, cmd):
        return """Command: {}  args: {},  kwargs: {}""".format(cmd, self.args, self.kwargs)

class Connection:
    def __init__(self, username, host, password, transport=None):
        self.username = username
        self.host = ip_address(host)
        self.password = password
        self.prompt = f"{username}@{host}"
        self.transport = transport

    def connect(self):
        self.transport = self.transport(self.username, self.host, self.password)
        return self
    
    def write(self, cmd):
        return self.transport.sendline(str(cmd))
    
    def clone(self):
        obj = copy.copy(self)
        obj.username = None
        obj.host = None
        obj.password = None
        obj.prompt = None
        return obj


class ConnectionBuilder:
    
    def __init__(self, connection, **attrs):
        self._connection = connection
        self.attrs = attrs
    
    @property
    def connection(self):
        return self._connection

    def set_username(self):
        name = 'username'
        if hasattr(self.connection, name):
            setattr(self.connection, name, self.attrs.get(name))
    
    def set_host(self):
        name = 'host'
        if hasattr(self.connection, name):
            setattr(self.connection, name, ip_address(self.attrs.get(name)))

    def set_prompt(self):
        name = 'prompt'
        if hasattr(self.connection, name):
            username = self.attrs.get('username')
            host = self.attrs.get('host')
            prompt = f"{username}@{host}"
            setattr(self.connection, name, prompt)
    
    def get_connection(self):
        return self._connection


class SingletonMeta(type):
    _instance = None
    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(SingletonMeta, cls).__call__(*args, **kwargs)
        return cls._instance
     


class PrototypedConnectionSingletonFactory(metaclass=SingletonMeta):

    def __init__(self, configs: Dict, connection: Connection, connection_builder: ConnectionBuilder):
        self.configs: Dict = configs
        self.connection_builder_cls: ConnectionBuilder = connection_builder
        self.connection_cls: Connection = connection

    def get_conn(self, **attrs: Dict) -> Connection:
        connection_instance = self.connection_cls(**self.configs[0])
        connection_prototype = connection_instance.clone()

        builder = self.connection_builder_cls(connection_prototype, **attrs)
        builder.set_username()
        builder.set_host()
        builder.set_prompt()

        connection = builder.get_connection()
        return connection


class InstanceConnectionSingletonFactory(PrototypedConnectionSingletonFactory):
    
   def get_conn(self, **attrs: Dict) -> Connection:
       return self.connection_cls(**attrs)




def main(factory, configs):

    factory_obj = factory(configs, Connection, ConnectionBuilder)    

    connections = (factory_obj.get_conn(**config) for config in configs)

    # Generator
    results = (connection.connect().write('show interface Vlan150') for connection in connections)

    # print(
    list(results)
    # )


if __name__ == '__main__':
    configs = [
        {
            'username': 'User1', 
            'host': '1.1.1.1', 
            'password': '1234Example', 
            'transport': Transport, 
        },
         {
            'username': 'User2', 
            'host': '2.2.2.2', 
            'password': '1234Example', 
            'transport': Transport, 
        },
        {
            'username': 'User3', 
            'host': '3.3.3.3', 
            'password': '1234Example', 
            'transport': Transport, 
        },
        
    ]

    factory = PrototypedConnectionSingletonFactory
    # factory = InstanceConnectionSingletonFactory

    # Timing main function excecution
    start = time.time()
    main(factory, configs * 1000)
    elapsed = time.time() - start

    print('\n\n elapsed time for {}'.format(factory.__name__))
    print(elapsed)

    
"""
python3 prototyped_singleton_factory_with_builder.py

elapsed time for PrototypedConnectionSingletonFactory
~ 0.08

elapsed time for InstanceConnectionSingletonFactory
~ 0.04

Its due to the fact that __copy__ operation in Python is heavier then __init__
"""
