

class ConnectionPool:

    def __init__(self, size):
        self._reusables = [Connection() for _ in range(size)]

    def acquire(self):
        return self._reusables.pop()

    def release(self, reusable):
        self._reusables.append(reusable)


class Connection:
    ...


def main():
    reusable_pool = ConnectionPool(10)
    reusable = reusable_pool.acquire()
    reusable_pool.release(reusable)


if __name__ == "__main__":
    main()
