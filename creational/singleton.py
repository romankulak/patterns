from __future__ import annotations
from typing import List, Tuple, Callable
from queue import Queue
from threading import Thread, Lock

def start_threads(*args):
    ...


class SingletonMeta(type):

    _instance: dict = {}
    _lock: Lock = Lock()
    
    def __call__(cls, *args, **kwargs):

        with cls._lock:
            if cls in cls._instances:            
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
            return cls._instances[cls]




class SingletonConnection(metaclass=SingletonMeta):
    def __init__(self) -> None:
        ...
    
    def connect(self):
        ...


class SingletonConnection2(SingletonMeta):
    ...


def create_vlan_action(parsed_args):
    tasks: List[Callable, Tuple] = [(SingletonConnection.create_vlan, (conf, *parsed_args.args)) for conf in parsed_args.configs]

    queue = Queue()
    start_threads(4, queue)
    for task in tasks:
        queue.put(task)
    queue.join()



"""
Usecase:
    Bulk Vlan create on device

In order not to create multiple connections we use Singleton connection with Lock 

"""
