from __future__ import annotations
from abc import ABC, abstractclassmethod, abstractmethod, abstractstaticmethod


class Connection:
    ...


class AbstractBuilder(ABC):
    @abstractmethod
    def reset():
        ...
    @abstractmethod
    def add_transport(self):
        ...
    @abstractmethod
    def add_processing_methods(self):
        ...
    
    @abstractmethod
    def add_representational_commands(self):
        ...

    @abstractmethod
    def add_configurational_commands(self):
        ...
    
    def get_connection(self) -> Connection:
        return self._connection


class AristaConnectionHBuilder(AbstractBuilder):
    ...

class JuniperConnectionHBuilder(AbstractBuilder):
    ...



class Director:
    def __init__(self, builder):
        self.builder = builder 

    def build_basic_conn(self):
        self.builder.add_transport()

    def build_representational_conn(self):
        self.builder.add_transport()
        self.builder.add_representational_commands()
        
    def build_configurational_conn(self):
        self.build_basic_conn()
        self.builder.add_configurational_commands()

    def build_full_conn(self):
        self.builder.add_transport()
        self.builder.add_configurational_commands()
        self.builder.add_representational_commands()

    def get_connection(self) -> Connection:
        return self.builder._connection

########################################

def create_vlan(builder):
    connection = (
        Director(builder())
        .build_configurational_conn()
        .get_connection()
    )

    connection.create_vlan()


def show_interface_status(builder):
    director = ShowDirector(builder)
    director.build_representational_conn()

    connection = builder.get_connection()
    connection.show_interface_status()




def create_vlan_contoller(conf):
    if conf.arista:
        builder = AristaConnectionHBuilder
    elif conf.juniper:
        builder = JuniperConnectionHBuilder
    else:
        raise Exception

    create_vlan(builder)

"""
Має сенс коли б різні типи виконуваних сценаріїв (create_vlan, show_interface ... ) потребвали (або ні) різних 'дорогих ресурсів'

Приклад будівельника? https://docs.celeryproject.org/en/stable/getting-started/next-steps.html#canvas-designing-work-flows

"""
