from abc import ABC, abstractclassmethod, abstractmethod

class AbstractConnection(ABC):
    @abstractmethod
    def connect(self, config):
        ...

class AbstractConnectionCreator(ABC):

    @abstractclassmethod
    def factory_method(cls, config: dict) -> AbstractConnection:
        ...


class LogicalConnecton(AbstractConnection):
    ...

class RealConnection(AbstractConnection):
    ...


class LogicalDeviceConnectionCreator(AbstractConnectionCreator):
    @classmethod
    def factory_method(cls, config: dict) -> LogicalConnecton:
        return LogicalConnecton(config)


class RealDeviceConnectionCreator(AbstractConnectionCreator):
    @classmethod
    def factory_method(cls, config: dict) -> RealConnection:
        return RealConnection(config)


############ Usage ########################


def action(creator: AbstractConnectionCreator, config: dict) -> str:
    connection = creator.factory_method(config)

    connection.connect()

    return connection.result()



class Controller:

    def take_action(self, args):

        if args.is_logical_device:
            creator = LogicalDeviceConnectionCreator
        else:
            creator = RealDeviceConnectionCreator

        result = action(creator, args.config)





